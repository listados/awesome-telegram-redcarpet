# awesome-telegram-Redcarpet
Proyecto que transforma el awesome-list de ["Listado de Canales, Grupos, Bots y Recursos de Telegram"](https://gitlab.com/listados/awesome-telegram) en una web en Gitlab Pages automaticamente.

## Pipeline
* Instala las dependencias para transformar el markdown a HTML.
  * [redcarpet](https://github.com/vmg/redcarpet)
* Recupera el `README.md` de [awesome-telegram](https://gitlab.com/listados/awesome-telegram/-/blob/master/README.md) con `curl`
* Se lanza un script en ruby (`.build_md.rb`).
  * Transforma el contenido y le da formato (estilo CSS) para convertirdo en una web.
* Coloca todos los ficheros resultantes en la carpeta `public`

### Script en ruby
* Peparar dos render y un elementos markdown, uno para el TOC y otro para el contenido.
* Abrir los ficheros correspondientes de lectura y escritura.
  * Leemos el `README.md` y lo procesamos.
  * Escribimos en HTML para la web y lo rellenamos con la transformación del elemento **markdown**.
  * Creamos el fichero de estilos con el CSS.

## Automatizar la actualización
Mediante un [trigger](https://docs.gitlab.com/ee/ci/triggers/) en el proyecto original se puede lanzar un rebuild.
* Crear un token (Pipeline triggers) en los ajustes de CI/CD de **este proyecto**
![repo_upstream](doc/Trigger.png "En el repositorio fuente")
* Configurar el trigger en el **proyecto original**
    * Crear una [variable](https://docs.gitlab.com/ee/ci/variables/README.html#create-a-custom-variable-in-the-ui) con el token protegido (**mask**)
    ![repo_downstream](doc/Variable.png "En este repositorio")
    * Añadir en lanzamiento del trigger en el `.gitlab-ci.yml`
      ```yaml
      trigger_build:
        stage: deploy
        script:
          - "curl -X POST -F token=$MRCTOKEN -F ref=master https://gitlab.com/api/v4/projects/18785384/trigger/pipeline"
      ```  

## Documentacion

* https://gitlab.com/9000-volts/pages-md-parse
* https://medium.com/faun/gitlab-pipeline-to-run-cross-multiple-projects-3563af5d6dca

## Alternativas 

* https://gitlab.com/pages/gitbook
* https://gitlab.com/yo/markdown-to-html

## Agradecimientos
* A Jose Jimenez del podcast y canal de Telegram [Aviones De Papel](https://t.me/avionesdepapel) por la sugerencia de pasar el listado a formato awesome-list,
* A @victorhck por la idea y el empujón para pasar el listado tambien a página web HTML para una mejor visualización,
* Y @9000-volts por su código que es en el que se basa este repo para transformar automaticamente el Markdown original.
