# Pages MD Parse - Public Domain
# This script generates a gitlab pages site from the markdown documents in the
# repository structure.
# NOTE: The URL of the page does NOT change for different markdown docs.

# !!! NOTE Change this configuration! !!!
title = 'Awesome Lists Redcarpet' # The title of the pages site
base = 'awesome-telegram-redcarpet' # The name of the repo (as used in the pages url)
repo = 'listados/awesome-telegram-redcarpet' # username/reponame on gitlab

require 'redcarpet'
require 'pygments'

# https://eng.rightscale.com/2015/10/14/custom-markdown-in-middleman.html
# Use pygments with red carpet.
class HTMLwithPygments < Redcarpet::Render::HTML
  def initialize(options={})
    super options.merge(:with_toc_data => true)
  end
  def block_code(code, language)
    Pygments.highlight(code, :lexer => language)
  end
end

#renderer = Redcarpet::Render::HTML.new(with_toc_data: true)
# Set up the markdown renderer.
markdown = Redcarpet::Markdown.new(HTMLwithPygments,
  no_intra_emphasis: true,
  tables: true,
  fenced_code_blocks: true,
  autolink: true,
  strikethrough: true,
  superscript: true,
  underline: true,
  highlight: true,
  quotes: true,
  footnotes: true
  )

toc_render = Redcarpet::Render::HTML_TOC.new(nesting_level: 2..3)
markdown_TOC = Redcarpet::Markdown.new(toc_render, fenced_code_blocks: true)


# Write each file's html equivalent.
input = File.open(Dir.pwd + "/README.md", "rb")
processed = markdown.render(input.read)
input.close()
output = File.open(Dir.pwd + "/README.md.html", "w")
output.write <<-eos
<!doctype html>
<html>
  <head>
    <meta charset="utf-8"/>
    <base target="_blank" />
    <title>Listado</title>
    <link rel="stylesheet" href="/#{base}/style.css"/>
  </head>
  <body>
    #{processed}
  </body>
</html>
eos
output.close()

# Write the index.
input = File.open(Dir.pwd + "/README.md", "rb")
processed_TOC = markdown_TOC.render(input.read)
input.close()
index = File.open(Dir.pwd + "/index.html", "w")
index.write <<-eos
<!doctype html>
<html>
  <head>
    <base target="frame" />
    <title>#{title}</title>
    <link rel="stylesheet" href="/#{base}/style.css"/>
    <style>
    body {
      margin: 0;
      display: flex;
      height: 100vh;
    }
    </style>
  </head>
  <body>
    <aside>
      <small>
        <a href="http://gitlab.com/#{repo}">#{repo}</a>
      </small>
      <h1>#{title}</h1>
      <small>
      #{processed_TOC.gsub('href="#', 'href="README.md.html#')}
      </small>
    </aside>
    <iframe src="/#{base}/README.md.html" name="frame"></iframe>
  </body>
</html>
eos
index.close()

# Write the css.
css = File.open(Dir.pwd + "/style.css", "w")
css.write <<-eos
* { box-sizing: border-box; }
body {
  background-color: #eFF0F1;
  font-family: sans-serif;
  color: #444;
  margin: 3em;
}
a {
  color: #2382c8;
  text-decoration: none;
}
a:hover { 
  color: #000;
  text-decoration: underline;
}
a:visited { color: #a0a; }
code{
  padding: 0 4px;
  font-family: "Source Code Pro", "Courier New", Courier, sans-serif;
  font-size: 0.9em;
  color: #556270;
  background: #F7E2E2;
  border-radius: 3px;
}
table {
  border-collapse: collapse;
  margin: 1em auto;
}
th, td {
  border: 1px solid #ccc;
  padding: 0.5em 0.7em;
}
iframe {
  width: 100%;
  border: none;
}
aside {
  width: 500px;
  border-right: 1px solid #ccc;
  padding: 1em;
}
aside>a {
  display: block;
  padding: 0.5em;
}
aside>ul {
  font-size: smaller;
}
eos
css.close()

